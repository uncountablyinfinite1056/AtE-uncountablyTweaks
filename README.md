# After the End uncountably's Tweaks

This is a submod for After the End Fan Fork (v0.5) containing, essentially, features I thought would be cool or interesting to add.
If you have any suggestions to make or bugs to report, feel free to use the GitLab issue tracker (preferred), or contact me on Discord (uncountablyInfinite#2944) or Reddit (u/uncountablyInfinit).
If there's enough demand, I'd be willing to split off features from this submod into their own individual submods.

# Installation Instructions
1. Click the download button in the top left of the page, and download the files as a .zip
2. Extract the .zip, and copy the folder into your CK2 mods folder (usually in Documents\Paradox Interactive\Crusader Kings II\mod)
3. Copy the .mod file out of the AtE-uncountablyTweaks folder and into the mod folder
4. Make sure you have both After the End Fan Fork v0.5 and After the End Renewed Ummah as well, both are needed for this mod to work properly

Alternatively, if you are familiar with git already, you can clone this repository into your mod folder and copy the .mod file out instead. This has the advantage of making it easier to update your local installation of this mod.

# Features
- An event chain for Paul Mahonic to retake his family's legacy
- Modifiers for different Americanists across different cultures, providing flavor and attribute changes
